# Stampy

Whether to forgive is your choice, but with `stampy`, you will *never* forget.
